﻿using System;

namespace SPRITSAMPLE.Model
{
	public class Journal
	{
        public int Oid { get; set; }
        public int? Signature { get; set; }
        public int? OptimisticLockField { get; set; }
        public string Data { get; set; }
        public string Text { get; set; }
    }
}
