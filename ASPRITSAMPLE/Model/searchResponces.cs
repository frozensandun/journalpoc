﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPRITSAMPLE.Model
{
    public class SearchResponces
    {
       public List<JournalDto> Result { get; set; }
        public int RowCount { get;set; }
    }
}
