﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPRITSAMPLE.Model
{
    public class SearchRequest
    {
        public string SortBy { get; set; }

        public bool? IsDesc { get; set; }

        public int? PageSize { get; set; }

        public int? PageIndex { get; set; }
    }
}
