﻿using ASPRITSAMPLE.Model;
using ASPRITSAMPLE.Services;
using Microsoft.AspNetCore.Mvc;

namespace ASPRITSAMPLE.Controllers
{
    [Route("api/[controller]")]
    public class JournalController : Controller
    {
        private IDataServices _services;
        public JournalController(IDataServices _services)
        {
            this._services = _services;
        }
   
        [HttpGet]
        public IActionResult Get(SearchRequest searchRequest)
        {
            return Ok(_services.JournalSearch(searchRequest));
        }

    }
}
