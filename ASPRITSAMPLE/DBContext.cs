﻿using Microsoft.EntityFrameworkCore;
using SPRITSAMPLE.Model;

namespace ASPRITSAMPLE
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options) { }

        public DbSet<Patient> Patients { get; set; }

        public DbSet<Journal> Journals { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Patient>(entity =>
            {
                entity.HasKey(e => e.Oid);
                entity.ToTable("HealthCare_Patient");
            });

            modelBuilder.Entity<Journal>(entity =>
            {
                entity.HasKey(e => e.Oid);
                entity.ToTable("Journals_Journal");
            });
        }


    }
}

