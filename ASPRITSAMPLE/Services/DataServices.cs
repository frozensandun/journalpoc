﻿using System.Collections.Generic;
using System.Linq;
using ASPRITSAMPLE.Model;

namespace ASPRITSAMPLE.Services
{
    public class DataServices : IDataServices
    {
        private readonly DBContext _context;
        private const int DEFAULT_PAGE_SIZE = 10;
        public DataServices(DBContext _context)
        {
            this._context = _context;
        }

        private IQueryable<JournalDto> Paging(IQueryable<JournalDto> query, int? pageIndex, int? pageSize)
        {
            if (pageIndex == null) return Enumerable.Empty<JournalDto>().AsQueryable();

            pageSize = pageSize != null ? pageSize.Value : DEFAULT_PAGE_SIZE;

            return query.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value);
        }

        public SearchResponces JournalSearch(SearchRequest searchRequest)
        {
            IQueryable<JournalDto> postalCodeSearchQuery = _context.Journals.Where(x=>x.Data != "" && !string.IsNullOrEmpty(x.Text))

                 .Select(x => new JournalDto()
                 {
                     FirstName = x.Text,
                     Oid = x.Oid
                 });

            var sortedQuery = postalCodeSearchQuery.OrderBy(q => q.Oid).AsQueryable();
            var query = Paging(sortedQuery, searchRequest.PageIndex, searchRequest.PageSize);

            return new SearchResponces()
            {
                RowCount = postalCodeSearchQuery.Count(),
                Result = query.ToList()
            };

        }


    }
}
