﻿using ASPRITSAMPLE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPRITSAMPLE.Services
{
    public interface IDataServices
    {
      SearchResponces JournalSearch(SearchRequest searchRequest);
    }
}
